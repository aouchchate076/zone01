package main

import (
	"fmt"
)

func main() {
	fmt.Printf("%#v\n", SplitWhiteSpaces("Hello how are you?"))
}

func SplitWhiteSpaces(s string) []string {
	sli := []string{}
	word := ""
	for _, char := range s {
		if char != ' ' {
			word += string(char)
		} else {
			sli = append(sli, word)
			word = ""
		}
	}
	if word != "" {
		sli = append(sli, word)
	}
	return sli
}
