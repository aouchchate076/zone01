package main

import (
	"fmt"
)

func main() {
	fmt.Println(TrimAtoi("12345"))
	fmt.Println(TrimAtoi("str123ing45"))
	fmt.Println(TrimAtoi("012 345"))
	fmt.Println(TrimAtoi("Hello World!"))
	fmt.Println(TrimAtoi("sd+x1fa2W3s4"))
	fmt.Println(TrimAtoi("sd-x1fa2W3s4"))
	fmt.Println(TrimAtoi("sdx1-fa2W3s4"))
	fmt.Println(TrimAtoi("sdx1+fa2W3s4"))
}

func TrimAtoi(s string) int {
	result := 0
	word := ""
	signe := 1
	for _, char := range s {
		if char == '-' && word == "" {
			word = word + string(char)
		}
		if char >= '0' && char <= '9' {
			word = word + string(char)
		} else {
			continue
		}
	}

	for i, char := range word {
		if i == 0 && char == '-' {
			signe = -1
			continue
		}
		if char >= '0' && char <= '9' {
			result = result*10 + int(char-'0')
		} else {
			return 0
		}
	}
	return result * signe
}
